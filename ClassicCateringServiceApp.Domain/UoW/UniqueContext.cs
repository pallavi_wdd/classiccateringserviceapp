﻿using ClassicCateringService.Domain.DbContexts;
using ClassicCateringServiceApp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.UoW
{
    public class UniqueContext : IDisposable
    {
        private CateringContext _context;
        private bool disposed;
        private CompanyRepository _companyRepository;
        private UserRepository _userRepository;
        private ProductRepository _productRepository;

        public UniqueContext()
        {
            if (_context == null)
            {
                _context = new CateringContext();
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public CateringContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new CateringContext();
                }
                return _context;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public CompanyRepository CompanyRepository
        {
            get
            {
                if (_companyRepository == null)
                {
                    _companyRepository = new CompanyRepository();
                }
                return _companyRepository;
            }
        }

        public ProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new ProductRepository();
                }
                return _productRepository;
            }
        }

        public UserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository();
                }
                return _userRepository;
            }
        }

        //public CategoryRepository CategoryRepository
        //{
        //    get
        //    {
        //        if (_categoryRepository == null)
        //        {
        //            _categoryRepository = new CategoryRepository();
        //        }
        //        return _categoryRepository;
        //    }
        //}

    }
}
