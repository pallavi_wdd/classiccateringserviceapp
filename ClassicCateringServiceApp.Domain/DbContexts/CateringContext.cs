﻿using ClassicCateringService.Domain.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.DbContexts
{
    public class CateringContext : IdentityDbContext
    {
        public CateringContext() : base("CateringConn") { }
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<DailySchedule> DailySchedules { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductRate> ProductRates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
           
            modelBuilder.Entity<ProductRate>()
               .HasRequired(c => c.Product)
               .WithMany()
               .HasForeignKey(c => c.ProductId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
             .HasRequired(c => c.Company)
             .WithMany(c=>c.Users)
             .HasForeignKey(c => c.CompanyId)
             .WillCascadeOnDelete(false);
        }
    }
}

