﻿using ClassicCateringService.Domain.DbContexts;
using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Repositories
{
    public class GenericRepository<TEntity> : IActionModule<TEntity> where TEntity : class
    {
        internal IResponse response;
        internal CateringContext _context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository()
        {
            _context = new CateringContext();
            dbSet = _context.Set<TEntity>();
            response = new Response();
        }

        public GenericRepository(CateringContext context)
        {
            _context = context;
            dbSet = _context.Set<TEntity>();
        }

        public virtual List<TEntity> GetAll()
        {
            return dbSet.ToList();
        }

        public virtual void Add(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual TEntity Find(object id)
        {
            return dbSet.Find(id);
        }

        public virtual void Cancel(TEntity entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public virtual void Edit(TEntity updateentity)
        {
            dbSet.Attach(updateentity);
            _context.Entry(updateentity).State = EntityState.Modified;
        }

        public virtual TEntity FirstOrDefault()
        {
            return dbSet.FirstOrDefault();
        }

        public virtual bool Any(
         Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                return query.Any(filter);
            }
            return false;
        }
    }
}
