﻿using AutoMapper;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Helper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Repositories
{
   public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public IResponse Save(UserView userview)
        {
            try
            {
                var user = Mapper.Map<User>(userview);
                #region Validation on the User View Data

                if (string.IsNullOrEmpty(user.Role))
                {
                    response.IsSuccess = false;
                    response.Message = "Please select valid role";
                    return response;
                }

                if (user.Role == "User")
                {
                    if (user.CompanyId <=0)
                    {
                        response.IsSuccess = false;
                        response.Message = "Please select company";
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(user.Salutation))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide salutation";
                    return response;
                }

                if (string.IsNullOrEmpty(user.FirstName))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide first name";
                    return response;
                }

                if (string.IsNullOrEmpty(user.LastName))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide last name";
                    return response;
                }

                if (string.IsNullOrEmpty(user.Gender))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide gender";
                    return response;
                }

                if (string.IsNullOrEmpty(user.ContactNumber))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide contact number";
                    return response;
                }

                if (string.IsNullOrEmpty(user.EmailAddress))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide email address";
                    return response;
                }
                user.UserName = string.Format("{0}.{1}", userview.FirstName.ToLower(), userview.LastName.ToLower());
                #endregion
                              
                var userStore = new UserStore<User>(_context);
                var manager = new UserManager<User>(userStore);
                var matchingUser = manager.FindByName(user.UserName);

                if (matchingUser == null)
                {
                    var newuser = new User
                    {
                        UserName = string.Format(userview.FirstName.ToLower() + "." + userview.LastName.ToLower()),
                        Role = userview.Role,
                        Salutation = userview.Salutation,
                        FirstName = userview.FirstName,
                        LastName = userview.LastName,
                        Gender = userview.Gender,
                        ContactNumber = userview.ContactNumber,
                        EmailAddress = userview.EmailAddress,
                        CompanyId = userview.CompanyId,
                        AdvanceAmount = userview.AdvanceAmount
                    };
                    var newresult = manager.Create(newuser, "pass@word1");
                    if (newresult.Succeeded)
                    {
                        manager.AddToRole(newuser.Id, userview.Role);
                        response.IsSuccess = true;
                        response.Data = newuser.Id;
                        response.Message = ExceptionHelper.GetSaveMessage("User");
                        _context.SaveChanges();
                        response.Message = ExceptionHelper.GetSaveMessage("User");
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = ExceptionHelper.GetDuplicateMessage("User");
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                response.IsSuccess = false;
                return response;
            }
        }
        public IResponse GetAllUsers()
        {
            var users = _context.Users.ToList();
            response.Data = Mapper.Map<UserView[]>(users);
            return response;
        }

        //public IResponse GetAllUsers()
        //{
        //    var user = _context.Roles.FirstOrDefault(c => c.Name == "User");
        //    var users = _context.Users.Where(c => c.Roles.Any(r => r.RoleId == user.Id)).ToList();
        //    response.Data = JsonConvert.SerializeObject(Mapper.Map<UserView[]>(users));
        //    return response;
        //}

        public IResponse GetUser(string id)
        {
            var user = _context.Users.Find(id);
            response.Data = Mapper.Map<UserView>(user);
            return response;
        }

        public IResponse Update(UserView userview)
        {
            try
            {
                User user = _context.Users.Find(userview.Id);
                if (string.IsNullOrEmpty(user.Role))
                {
                    response.IsSuccess = false;
                    response.Message = "Please select valid role";
                    return response;
                }

                if (user.Role == "User")
                {
                    if (user.CompanyId <= 0)
                    {
                        response.IsSuccess = false;
                        response.Message = "Please select company";
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(user.Salutation))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide salutation";
                    return response;
                }

                if (string.IsNullOrEmpty(user.FirstName))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide first name";
                    return response;
                }

                if (string.IsNullOrEmpty(user.LastName))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide last name";
                    return response;
                }

                if (string.IsNullOrEmpty(user.Gender))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide gender";
                    return response;
                }

                if (string.IsNullOrEmpty(user.ContactNumber))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide contact number";
                    return response;
                }

                if (string.IsNullOrEmpty(user.EmailAddress))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide email address";
                    return response;
                }
                user.CompanyId = userview.CompanyId;
                user.Salutation = userview.Salutation;
                user.FirstName = userview.FirstName;
                user.LastName = userview.LastName;
                user.ContactNumber = userview.ContactNumber;
                user.EmailAddress = userview.EmailAddress;
                user.Gender = userview.Gender;
                user.Role = userview.Role;
                user.AdvanceAmount = userview.AdvanceAmount;

                var userStore = new UserStore<User>(_context);
                var manager = new UserManager<User>(userStore);
                manager.Update(user);

                if (manager.IsInRole(user.Id, "Admin"))
                {
                    manager.AddToRole(user.Id, "Admin");
                }

                if (manager.IsInRole(user.Id, "User"))
                {
                    manager.AddToRole(user.Id, "User");
                }

                if(manager.IsInRole(user.Id, "Vendor"))
                {
                    manager.AddToRole(user.Id, "Vendor");
                }

                response.IsSuccess = true;
                response.Message = ExceptionHelper.GetUpdateMessage("User");
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                return response;
            }
        }

        public IResponse Delete(string id)
        {
            try
            {
                var user = _context.Users.Find(id);
                if (user != null)
                {
                    _context.Users.Remove(user);
                    _context.SaveChanges();
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetDeleteMessage("Record");
                }
                response.Data = Mapper.Map<UserView>(user);
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtDeleteMessage();
                return response;
            }
        }

        public IResponse ChangePassword(string username, string oldpassword, string newpassword)
        {
            var userStore = new UserStore<User>(_context);
            var manager = new UserManager<User>(userStore);
            User user = manager.Find(username, oldpassword);

            if (user != null)
            {
                var result = manager.ChangePassword(user.Id, oldpassword, newpassword);
                if (result.Succeeded)
                {
                    System.Threading.Tasks.Task task = manager.UpdateAsync(user);
                    task.Wait();
                    //_context.Entry(vendor).State = EntityState.Modified;

                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetChangedPasswordMessage("Your password");
                    return response;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Unable to change password. Contact Administrator";
                    return response;
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Please provide valid user name and password";
                return response;
            }
        }

        public IResponse ForgotPassword(string contactnumber)
        {
            try
            {
                var user = _context.Users.FirstOrDefault(c => c.ContactNumber == contactnumber);
                if (user != null)
                {
                    var store = new UserStore<User>(_context);
                    UserManager<User> usermanager = new UserManager<User>(store);
                    User currentuser = usermanager.FindById(user.Id);
                    System.Threading.Tasks.Task task = usermanager.UpdateAsync(currentuser);
                    task.Wait();
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.SentOtpMessage();
                }
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "User does not exist with provided mobile number";
                return response;
            }
        }

        public IResponse ResetPassword(string contactnumber, string newpassword)
        {
            var user = _context.Users.FirstOrDefault(c => c.ContactNumber == contactnumber);

            if (user != null)
            {
                var store = new UserStore<User>(_context);
                UserManager<User> usermanager = new UserManager<User>(store);
                user.PasswordHash = usermanager.PasswordHasher.HashPassword(newpassword);
                System.Threading.Tasks.Task task = usermanager.UpdateAsync(user);
                task.Wait();
                if (task.IsCompleted)
                {
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetPasswordResetMessage();
                    return response;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Unable to change password. Please contact administrator";
                    return response;
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Please provide valid user name and password";
                return response;
            }
        }
    }
}
