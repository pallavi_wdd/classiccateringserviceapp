﻿using AutoMapper;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Helper;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Repositories
{
   public class DailyScheduleRepository : GenericRepository<DailySchedule>, IDailyScheduleRepository
    {
        public IResponse Save(DailyScheduleView scheduleview)
        {
            try
            {
                var schedule = Mapper.Map<DailySchedule>(scheduleview);

                if (schedule.ComanyId <=0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please select company";
                    return response;
                }

                if (schedule.ProductId <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please select product";
                    return response;
                }

                if (string.IsNullOrEmpty(scheduleview.SDate))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide date";
                    return response;
                }

                if (schedule.StartTime <=0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide start time";
                    return response;
                }

                if (schedule.EndTime <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide end time";
                    return response;
                }

                if (schedule.Rate <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide rate";
                    return response;
                }

                if (dbSet.Any(c => c.ComanyId == schedule.ComanyId && c.ProductId == schedule.ProductId && c.Date == c.Date))
                {
                    response.IsSuccess = false;
                    response.Message = "Record already exist";
                    return response;
                }
                _context.DailySchedules.Add(schedule);
                _context.SaveChanges();
                response.IsSuccess = true;
                response.Data = Mapper.Map<DailyScheduleView>(schedule);
                response.Message = ExceptionHelper.GetSaveMessage("Category");
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                response.IsSuccess = false;
                return response;
            }
        }
        public IResponse GetAllDailySchedules()
        {
            var categories = _context.Categories.ToList();
            response.Data = Mapper.Map<CategoryView[]>(categories);
            return response;
        }
        public IResponse GetSchdeule(Int64 id)
        {
            var category = _context.Categories.Find(id);
            response.Data = Mapper.Map<CategoryView>(category);
            return response;
        }
        public IResponse Update(DailyScheduleView scheduleview)
        {
            try
            {
                var schedule = Mapper.Map<DailySchedule>(scheduleview);

                if (schedule.ComanyId <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please select company";
                    return response;
                }

                if (schedule.ProductId <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please select product";
                    return response;
                }

                if (string.IsNullOrEmpty(scheduleview.SDate))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide date";
                    return response;
                }

                if (schedule.StartTime <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide start time";
                    return response;
                }

                if (schedule.EndTime <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide end time";
                    return response;
                }

                if (schedule.Rate <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide rate";
                    return response;
                }

                if (dbSet.Any(c => c.ComanyId == schedule.ComanyId && c.ProductId == schedule.ProductId && 
                c.Date == c.Date))
                {
                    response.IsSuccess = false;
                    response.Message = "Record already exist";
                    return response;
                }

                _context.DailySchedules.Attach(schedule);
                _context.Entry(schedule).State = EntityState.Modified;
                _context.SaveChanges();
                response.Data = Mapper.Map<CategoryView>(schedule);
                response.IsSuccess = true;
                response.Message = ExceptionHelper.GetUpdateMessage("Record");
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                return response;
            }
        }
        public IResponse Delete(Int64 id)
        {
            try
            {
                var schedule = _context.DailySchedules.Find(id);
                if (schedule != null)
                {
                    _context.DailySchedules.Remove(schedule);
                    _context.SaveChanges();
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetDeleteMessage("Record");
                }
                response.Data = schedule;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtDeleteMessage();
                return response;
            }
        }
    }
}
