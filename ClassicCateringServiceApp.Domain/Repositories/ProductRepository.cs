﻿using AutoMapper;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Helper;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Repositories
{
   public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public IResponse Save(ProductView productview)
        {
            try
            {
                var Product = Mapper.Map<Product>(productview);
                if (string.IsNullOrEmpty(Product.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide product name";
                    return response;
                }

                if (Product.Quantity<=0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide product quantity";
                    return response;
                }

                if (Product.Weight <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide product weight";
                    return response;
                }

                if (Product.Rate <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide product rate";
                    return response;
                }

                if (dbSet.Any(c => c.Name == Product.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Record already exist";
                    return response;
                }
                _context.Products.Add(Product);
                _context.SaveChanges();
                response.IsSuccess = true;
                response.Data = Mapper.Map<ProductView>(Product);
                response.Message = ExceptionHelper.GetSaveMessage("Product");
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                response.IsSuccess = false;
                return response;
            }
        }

        public IResponse GetAllProducts()
        {
            var Products = _context.Products.ToList();
            response.Data = Mapper.Map<ProductView[]>(Products);
            return response;
        }

        public IResponse GetProduct(Int64 id)
        {
            var Product = _context.Products.Find(id);
            response.Data = Mapper.Map<ProductView>(Product);
            return response;
        }

        public IResponse Update(ProductView Productview)
        {
            try
            {
                var Product = Mapper.Map<Product>(Productview);
                if (string.IsNullOrEmpty(Product.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide main product name";
                    return response;
                }

                if (Product.Quantity <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide main product quantity";
                    return response;
                }

                if (Product.Weight <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide main product weight";
                    return response;
                }

                if (Product.Rate <= 0)
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide main product rate";
                    return response;
                }

                if (dbSet.Any(c => c.Name == Product.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Record already exist";
                    return response;
                }
                _context.Products.Attach(Product);
                _context.Entry(Product).State = EntityState.Modified;
                _context.SaveChanges();
                response.Data = Mapper.Map<ProductView>(Product);
                response.IsSuccess = true;
                response.Message = ExceptionHelper.GetUpdateMessage("Record");
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                return response;
            }

        }

        public IResponse Delete(Int64 id)
        {
            try
            {
                var Product = _context.Products.Find(id);
                if (Product != null)
                {
                    _context.Products.Remove(Product);
                    _context.SaveChanges();
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetDeleteMessage("Record");
                }
                response.Data = Product;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtDeleteMessage();
                return response;
            }
        }
    }
}
