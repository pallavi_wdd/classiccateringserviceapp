﻿using AutoMapper;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.Interfaces.IRepositories;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Repositories
{
   public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public IResponse Save(CategoryView categoryview)
        {
            try
            {
                var category = Mapper.Map<Category>(categoryview);
                var starttimespan = TimeSpan.FromHours(category.StartTime);
                var endtimespan = TimeSpan.FromHours(category.EndTime);

                if (string.IsNullOrEmpty(category.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide category name";
                    return response;
                }

                if (dbSet.Any(c => c.Name == category.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Record already exist";
                    return response;
                }
                _context.Categories.Add(category);
                _context.SaveChanges();
                response.IsSuccess = true;
                response.Data = Mapper.Map<CategoryView>(category);
                response.Message = ExceptionHelper.GetSaveMessage("Category");
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                response.IsSuccess = false;
                return response;
            }
        }

        public IResponse GetAllCategories()
        {
            var categories = _context.Categories.ToList();
            response.Data = Mapper.Map<CategoryView[]>(categories);
            return response;
        }

        public IResponse GetCategory(Int64 id)
        {
            var category = _context.Categories.Find(id);
            response.Data = Mapper.Map<CategoryView>(category);
            return response;
        }
        public IResponse Update(CategoryView categoryview)
        {
            try
            {
                var category = Mapper.Map<Category>(categoryview);
                var timespan = TimeSpan.FromHours(category.StartTime);

                if (string.IsNullOrEmpty(category.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide category name";
                    return response;
                }

                if (dbSet.Any(c => c.Name == category.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Record already exist";
                    return response;
                }
                _context.Categories.Attach(category);
                _context.Entry(category).State = EntityState.Modified;
                _context.SaveChanges();
                response.Data = Mapper.Map<CategoryView>(category);
                response.IsSuccess = true;
                response.Message = ExceptionHelper.GetUpdateMessage("Record");
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                return response;
            }
        }

        public IResponse Delete(Int64 id)
        {
            try
            {
                var category = _context.Categories.Find(id);
                if (category != null)
                {
                    _context.Categories.Remove(category);
                    _context.SaveChanges();
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetDeleteMessage("Record");
                }
                response.Data = category;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtDeleteMessage();
                return response;
            }
        }
    }
}
