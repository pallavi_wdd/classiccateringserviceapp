﻿using AutoMapper;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Helper;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using ClassicCateringServiceApp.Domain.UoW;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClassicCateringServiceApp.Domain.Repositories
{
    public class CompanyRepository: GenericRepository<Company>, ICompanyRepository
    {
        private UniqueContext uc = new UniqueContext();
        public IResponse Save(CompanyView companyview)
        {
            try
            {
                var company = Mapper.Map<Company>(companyview);

                if (string.IsNullOrEmpty(company.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company name";
                    return response;
                }
                
                if (string.IsNullOrEmpty(company.Address))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company address";
                    return response;
                }

                if (string.IsNullOrEmpty(company.Location))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company location";
                    return response;
                }

                if (string.IsNullOrEmpty(company.ContactNumber))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company contact number";
                    return response;
                }

                if (uc.CompanyRepository.Any(c => c.Name == company.Name && c.Location == company.Location))
                {
                    response.IsSuccess = false;
                    response.Message = "This company alreday exists";
                    return response;
                }

                _context.Companies.Add(company);
                _context.SaveChanges();
                response.IsSuccess = true;
                response.Data = Mapper.Map<CompanyView>(company);
                response.Message = ExceptionHelper.GetSaveMessage("Category");
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                response.IsSuccess = false;
                return response;
            }
        }

        public IResponse GetAllCompanies()
        {
            var companies = _context.Companies.ToList();
            response.Data = Mapper.Map<CompanyView[]>(companies);
            return response;
        }

        public IResponse GetCompany(Int64 id)
        {
            var company = _context.Companies.Find(id);
            response.Data = Mapper.Map<CompanyView>(company);
            return response;
        }

        public IResponse Update(CompanyView companyview)
        {
            try
            {
                var company = Mapper.Map<Company>(companyview);

                if (string.IsNullOrEmpty(company.Name))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company name";
                    return response;
                }

                if (string.IsNullOrEmpty(company.Address))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company address";
                    return response;
                }

                if (string.IsNullOrEmpty(company.Location))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company location";
                    return response;
                }

                if (string.IsNullOrEmpty(company.ContactNumber))
                {
                    response.IsSuccess = false;
                    response.Message = "Please provide company contact number";
                    return response;
                }

                _context.Entry(company).State = EntityState.Modified;
                _context.SaveChanges();

                response.IsSuccess = true;
                response.Message = ExceptionHelper.GetUpdateMessage("Record");
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtSaveMessage(ex);
                return response;
            }
        }

        public IResponse Delete(Int64 id)
        {
            try
            {
                var company = _context.Companies.Find(id);
                if (company != null)
                {
                    _context.Companies.Remove(company);
                    _context.SaveChanges();
                    response.IsSuccess = true;
                    response.Message = ExceptionHelper.GetDeleteMessage("Record");
                }
                response.Data = company;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ExceptionHelper.GetErrorAtDeleteMessage();
                return response;
            }
        }
    }
}
