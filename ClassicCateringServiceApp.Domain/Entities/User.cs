﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Entities
{
    public class User: IdentityUser
    {
        public string Role { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public decimal AdvanceAmount { get; set; }
        public virtual Company Company { get; set; }
        public Int64? CompanyId { get; set; }
    }
}
