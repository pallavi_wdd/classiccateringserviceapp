﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Entities
{
    public class ProductRate:BaseEntity
    {
        public virtual Company Company { get; set; }
        public Int64 ComanyId { get; set; }
        public virtual Product Product { get; set; }
        public Int64 ProductId { get; set; }
        public double Rate { get; set; }
    }
}
