﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Entities
{
   public class DailySchedule :BaseEntity
    {
        public virtual Company Company { get; set; }
        public Int64 ComanyId { get; set; }
        public virtual Product Product { get; set; }
        public Int64 ProductId { get; set; }
        public decimal StartTime { get; set; }
        public decimal EndTime { get; set; }
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }
        public bool IsSpecial { get; set; }
    }
}
