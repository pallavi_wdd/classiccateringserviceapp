﻿using ClassicCateringService.Domain.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Entities
{
   public class BaseEntity
    {
        public BaseEntity()
        {
            CreatedDateTime = DateTimeHelper.LocalDateTime;
            LastModifiedDateTime = DateTimeHelper.LocalDateTime;
            Active = true;
        }
        public Int64 Id { get; set; }
        public bool Active { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public Int64? CreatedById { get; set; }
        public DateTime? LastModifiedDateTime { get; set; }
        public Int64? LastModifiedById { get; set; }
    }
}
