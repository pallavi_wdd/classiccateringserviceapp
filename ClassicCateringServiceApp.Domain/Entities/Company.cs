﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Entities
{
    public class Company:BaseEntity
    {
        public Company()
        {
            Users = new List<User>();
        }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string ContactNumber { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
