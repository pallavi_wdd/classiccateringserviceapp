﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Interfaces
{
    public interface IActionModule<TEntity> where TEntity : class
    {
        List<TEntity> GetAll();
        void Add(TEntity entity);
        void Cancel(TEntity entity);
        void Edit(TEntity entity);
    }
}
