﻿using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Interfaces.IRepositories
{
    public interface IProductRepository
    {
        IResponse Save(ProductView productview);
        IResponse GetAllProducts();
        IResponse GetProduct(Int64 id);
        IResponse Update(ProductView productview);
        IResponse Delete(Int64 id);

    }
}
