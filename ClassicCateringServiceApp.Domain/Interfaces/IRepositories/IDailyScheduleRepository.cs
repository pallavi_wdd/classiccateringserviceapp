﻿using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Interfaces.IRepositories
{
    public interface IDailyScheduleRepository
    {
        IResponse Save(DailyScheduleView dailyscheduleview);
        IResponse GetAllDailySchedules();
        IResponse GetSchdeule(Int64 id);
        IResponse Update(DailyScheduleView dailyscheduleview);
        IResponse Delete(Int64 id);
    }
}
