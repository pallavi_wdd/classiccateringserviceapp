﻿using ClassicCateringService.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Interfaces.IRepositories
{
    public interface ICategoryRepository
    {
        IResponse Save(CategoryView categoryview);
        IResponse GetAllCategories();
        IResponse GetCategory(Int64 id);
        IResponse Update(CategoryView categoryview);
        IResponse Delete(Int64 id);

    }
}
