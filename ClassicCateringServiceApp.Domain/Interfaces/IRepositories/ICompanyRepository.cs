﻿using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Interfaces.IRepositories
{
    public interface ICompanyRepository
    {
        IResponse Save(CompanyView companyview);
        IResponse GetAllCompanies();
        IResponse GetCompany(Int64 id);
        IResponse Update(CompanyView companyview);
        IResponse Delete(Int64 id);
    }
}
