﻿using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Repositories
{
    public interface IUserRepository
    {
        IResponse Save(UserView userview);

        //IResponse GetAllVendors();

        IResponse GetAllUsers();

        IResponse GetUser(string id);

        IResponse Update(UserView userview);

        IResponse Delete(string id);

        IResponse ChangePassword(string username, string oldpassword, string newpassword);

        IResponse ForgotPassword(string contactnumber);

        IResponse ResetPassword(string contactnumber, string newpassword);
    }
}
