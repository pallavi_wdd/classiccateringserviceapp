﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringServiceApp.Domain.Helper
{
   public class ExceptionHelper
    {
        public static string GetSuccessMessage(string type)
        {
            return string.Format("{0} add successfully!", type);
        }

        public static string GetSaveMessage(string type)
        {
            return string.Format("{0} saved successfully!", type);
        }

        public static string GetUpdateMessage(string type)
        {
            return string.Format("{0} updated successfully!", type);
        }

        public static string GetDuplicateMessage(string type)
        {
            return string.Format("{0} already exist", type);
        }

        public static string GetDeleteMessage(string type)
        {
            return string.Format("{0} successfully deleted!", type);
        }

        public static string GetErrorAtDeleteMessage()
        {
            return "Unable to delete record";
        }

        public static string NullUserMessage()
        {
            return "Please login";
        }

        public static string GetChangedPasswordMessage(string type)
        {
            return string.Format("{0} changed successfully!", type);
        }

        public static string SentOtpMessage()
        {
            return string.Format("OTP has been sent on your registered mobile number");
        }

        public static string GetActivateMessage(string type)
        {
            return string.Format("{0} is activated successfully!", type);
        }

        public static string GetDeactivateMessage(string type)
        {
            return string.Format("{0} is deactivated!", type);
        }

        public static string GetPasswordResetMessage()
        {
            return string.Format("Your password changed successfully!");
        }

        public static string GetErrorAtSaveMessage(Exception ex)
        {
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }
            if (ex.InnerException == null)
            {
                return "Unable to save record, Error-" + ex.Message;
            }

            return "Unable to save record";
        }

        //for test cases messages
        public static string GetRequiredDataMessage(string data)
        {
            return string.Format("Please provide {0}", data);
        }

        public static string GetSelectionMessage(string data)
        {
            return string.Format("Please select valid {0}", data);
        }
    }
}
