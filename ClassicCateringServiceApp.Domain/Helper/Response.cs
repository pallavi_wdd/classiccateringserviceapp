﻿using ClassicCateringService.Domain.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.Helper
{
    public class Response : IResponse
    {
        public bool IsSuccess { get; set; }
        public bool IsDeleted { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public Int64 LastSavedId { get; set; }

        public string GetResponse()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static string GetSaveMessage()
        {
            return "Record save successfully";
        }

        public static string GetDuplicateMessage()
        {
            return "Duplicate Record exist";
        }

        public static string GetDeleteMessage()
        {
            return "Record deleted successfully";
        }
        public static string GetNotFoundMessage()
        {
            return "Invalid record selected";
        }
        public static string GetUpdateMessage()
        {
            return "Record updated successfully";
        }

        public static string GetErrorAtSaveMessage(Exception ex)
        {
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }
            if (ex.InnerException == null)
            {
                return "Unable to save record, Error-" + ex.Message;
            }

            return "Unable to save record";
        }
    }
}
