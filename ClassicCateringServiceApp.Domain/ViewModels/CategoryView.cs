﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.ViewModels
{
   public class CategoryView :BaseEntityView
    {
        public string Name { get; set; }
        public double StartTime { get; set; }
        public double EndTime { get; set; }
    }
}
