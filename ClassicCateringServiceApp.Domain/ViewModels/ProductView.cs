﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.ViewModels
{
    public class ProductView:BaseEntityView
    {
        public string Name { get; set; }
        //per plate
        public decimal Quantity { get; set; }
        //per piece
        public decimal Weight { get; set; }
        public decimal Rate { get; set; }
    }
}
