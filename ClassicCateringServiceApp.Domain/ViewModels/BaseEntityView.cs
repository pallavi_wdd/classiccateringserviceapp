﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.ViewModels
{
   public class BaseEntityView
    {
        public BaseEntityView()
        {
            Active = true;
        }
        public Int64 Id { get; set; }
        public bool Active { get; set; }
    }
}
