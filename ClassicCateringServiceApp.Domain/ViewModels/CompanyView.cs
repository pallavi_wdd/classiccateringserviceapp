﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.ViewModels
{
    public class CompanyView :BaseEntityView
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string ContactNumber { get; set; }
        public ICollection<UserView> Users { get; set; }
    }
}
