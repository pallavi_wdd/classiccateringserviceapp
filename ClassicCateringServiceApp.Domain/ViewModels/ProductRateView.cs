﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassicCateringService.Domain.ViewModels
{
   public class ProductRateView :BaseEntityView
    {
        public Int64 ComanyId { get; set; }
        public Int64 ProductId { get; set; }
        public double Rate { get; set; }
    }
}
