﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClassicCateringServiceApp.Startup))]
namespace ClassicCateringServiceApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
