﻿using AutoMapper;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ClassicCateringServiceApp.App_Start
{
    public class AutoMapperStartup
    {
        private static CultureInfo culture = new CultureInfo("en-IN");
        private static string dateformat = "MM/dd/yyyy";
        private static string dateformat_display = "dd/MM/yyyy";

        public static void Map()
        {
            Mapper.CreateMap<Company, CompanyView>();
            Mapper.CreateMap<CompanyView, Company>();

            Mapper.CreateMap<User, UserView>()
                .ForMember(c => c.CompanyName, b => b.MapFrom(a => a.Company.Name + " - " +a.Company.Location));
            Mapper.CreateMap<UserView, User>();

            Mapper.CreateMap<BaseEntity, BaseEntityView>();
            Mapper.CreateMap<BaseEntityView, BaseEntity>();

            Mapper.CreateMap<Product, ProductView>();
            Mapper.CreateMap<ProductView, Product>();

            Mapper.CreateMap<Category, CategoryView>();
            Mapper.CreateMap<CategoryView, Category>();
        }
    }
}