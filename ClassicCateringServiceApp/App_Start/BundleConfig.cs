﻿using System.Web;
using System.Web.Optimization;

namespace ClassicCateringServiceApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-2.1.4.min.js",
                          "~/Scripts/jqbrowser-compressed.js",
                           "~/Scripts/bootstrap/bootstrap.min.js",
                            "~/Scripts/bootstrap-datepicker.min.js",
                            "~/Scripts/moment.min.js",
                           "~/Scripts/jquery.maskedinput.min.js",
                           "~/Scripts/chosen.jquery.js",
                           "~/Scripts/jquery-ui.min.js",
                           "~/Scripts/underscore-min.js",
                           "~/Scripts/shortcut.js",
                           "~/Scripts/highcharts.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                                   "~/Scripts/angular/angular.min.js",
                                   "~/Scripts/angular/angular-route.min.js",
                                   "~/Scripts/angular/angular-ui-router.js",
                                   "~/Scripts/angular/angular-ui-router.min.js",
                                    "~/Scripts/angular-localForage.js",
                                   "~/Scripts/localforage.js",
                                    "~/Scripts/bootstrap-datepicker.min.js",
                                    "~/Scripts/moment.js",
                                    "~/Scripts/notifIt.js",
                                    "~/Scripts/ProgressBar/loading-bar.js"));

            bundles.Add(new ScriptBundle("~/bundles/fileuploadjs").Include(
                                  "~/Scripts/ng-file-upload-all.min.js",
                                  "~/Scripts/ng-file-upload-shim.min.js",
                                  "~/Scripts/ng-file-upload.min.js"
                                  ));

            bundles.Add(new ScriptBundle("~/bundles/angularapp").Include(
              "~/Appscript/CateringServiceApp.js",
              "~/Appscript/Service.js",

               //Admin Module
               "~/Appscript/Admin/company/companycontroller.js",
                "~/Appscript/Admin/user/usercontroller.js",
                "~/Appscript/Admin/Product/productcontroller.js",
               "~/Appscript/Admin/Category/categorycontroller.js"

               //User Module

               ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/custom.css",
                      "~/Content/css/notifIt.css"));
        }
    }
}
