namespace ClassicCateringServiceApp.Migrations
{
    using ClassicCateringService.Domain.DbContexts;
    using ClassicCateringService.Domain.Entities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CateringContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CateringContext context)
        {
            var userStore = new UserStore<User>(context);
            var manager = new UserManager<User>(userStore);

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            List<string> roles = new List<string> { "Admin", "Vendor", "User" };
            IdentityResult roleResult;

            // Check to see if Role Exists, if not create it
            foreach (var role in roles)
            {
                if (!RoleManager.RoleExists(role))
                {
                    roleResult = RoleManager.Create(new IdentityRole(role));
                }
            }

            User user = new User
            {
                UserName = "Admin",
                EmailAddress = "pallavi@wddteam.com"
            };

            var result = manager.Create(user, "pass@word1");
            if (result.Succeeded)
            {
                manager.AddToRole(user.Id, "Admin");
            }
            context.SaveChanges();
        }
    }
}
