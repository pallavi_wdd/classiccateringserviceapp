﻿using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using ClassicCateringServiceApp.Domain.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassicCateringServiceApp.Controllers
{
    public class ProductController : Controller
    {
        private IResponse response;
        private IProductRepository productRepository;

        public ProductController()
        {
            productRepository = new ProductRepository();
            response = new Response();
        }

        [Authorize]
        [HttpGet]
        public string Index()
        {
            var products = productRepository.GetAllProducts();
            return products.GetResponse();
        }

        [HttpPost]
        [Authorize]
        public string Add(ProductView productview)
        {
            try
            {
                response = productRepository.Save(productview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response.GetResponse();
            }
        }

        [HttpPost]
        [Authorize]
        public string Edit(Int64 id)
        {
            try
            {
                response = productRepository.GetProduct(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Update(ProductView productview)
        {
            try
            {
                response = productRepository.Update(productview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }


        [HttpPost]
        [Authorize]
        public string Delete(Int64 id)
        {
            try
            {
                response = productRepository.Delete(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }
    }
}
