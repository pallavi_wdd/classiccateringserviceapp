﻿using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.Interfaces.IRepositories;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassicCateringServiceApp.Controllers
{
    public class UserController : Controller
    {
        private IResponse response;
        private IUserRepository userRepository;
        public UserController()
        {
            userRepository = new UserRepository();
            response = new Response();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public string Index()
        {
            var response = userRepository.GetAllUsers();
            return response.GetResponse();
        }

        //[Authorize(Roles = "Admin")]
        //[HttpGet]
        //public string GetAllUsers()
        //{
        //    var response = userRepository.GetAllUsers();
        //    return response.GetResponse();
        //}

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public string Save(UserView userview)
        {
            try
            {
                response = userRepository.Save(userview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Edit(string id)
        {
            try
            {
                response = userRepository.GetUser(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Update(UserView userview)
        {
            try
            {
                response = userRepository.Update(userview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }


        [HttpPost]
        [Authorize]
        public string Delete(string id)
        {
            try
            {
                response = userRepository.Delete(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        public string ChangePassword(string username, string oldpassword, string newpassword)
        {
            try
            {
                response = userRepository.ChangePassword(username, oldpassword, newpassword);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        public string ForgotPassword(string contactnumber)
        {
            try
            {
                response = userRepository.ForgotPassword(contactnumber);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        public string ResetPassword(string contactnumber, string newpassword)
        {
            try
            {
                response = userRepository.ResetPassword(contactnumber, newpassword);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }
    }
}