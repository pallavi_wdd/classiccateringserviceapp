﻿using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using ClassicCateringServiceApp.Domain.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassicCateringServiceApp.Controllers
{
    public class CompanyController : Controller
    {
        private IResponse response;
        private ICompanyRepository companyRepository;

        public CompanyController()
        {
            companyRepository = new CompanyRepository();
            response = new Response();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public string Index()
        {
            var companies = companyRepository.GetAllCompanies();
            return companies.GetResponse();
        }

        [HttpPost]
        [Authorize]
        public string Add(CompanyView companyview)
        {
            try
            {
                response = companyRepository.Save(companyview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response.GetResponse();
        }

        [HttpPost]
        [Authorize]
        public string Edit(Int64 id)
        {
            try
            {
                response = companyRepository.GetCompany(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Update(CompanyView companyview)
        {
            try
            {
                response = companyRepository.Update(companyview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response.GetResponse();
        }

        [HttpPost]
        [Authorize]
        public string Delete(Int64 id)
        {
            try
            {
                response = companyRepository.Delete(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }
    }
}