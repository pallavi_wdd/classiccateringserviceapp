﻿using AutoMapper;
using ClassicCateringService.Domain.DbContexts;
using ClassicCateringService.Domain.Entities;
using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassicCateringServiceApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly CateringContext _Context = new CateringContext();
        private readonly Response _response = new Response();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string GetCurrentUser()
        {
            var currentuser = HttpContext.User.Identity.GetUserId();
            var user = _Context.Users.FirstOrDefault(c => c.Id == currentuser);

            return JsonConvert.SerializeObject(Mapper.Map<UserView>(user));
        }

        [Authorize]
        [HttpPost]
        public string GetLoggedUser()
        {
            var userStore = new UserStore<User>(_Context);
            var manager = new UserManager<User>(userStore);
            User user = manager.FindById(HttpContext.User.Identity.GetUserId());
            if (user != null)
            {
                string Role = manager.IsInRole(user.Id, "Admin") ? "Admin" :
              manager.IsInRole(user.Id, "Vendor") ? "Vendor" : "NA";

                if (Role == "Vendor")
                {
                    var vendor = _Context.Users.FirstOrDefault(c => c.Id == user.Id);
                    var CurentUser = new
                    {
                        user.FirstName,
                        user.LastName,
                        user.Id,
                        Salutation = user.Salutation,
                        Role = Role,
                        user.ContactNumber,
                        UserName = user.UserName
                    };
                    return JsonConvert.SerializeObject(CurentUser);
                }
                else
                {
                    var CurentUser = new
                    {
                        user.FirstName,
                        user.LastName,
                        user.Id,
                        Salutation = user.Salutation,
                        Role = Role,
                        user.ContactNumber,
                        UserName = user.UserName
                    };
                    return JsonConvert.SerializeObject(CurentUser);
                }
            }
            return "";
        }

    }
}