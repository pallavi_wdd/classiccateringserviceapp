﻿using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.Interfaces.IRepositories;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using ClassicCateringServiceApp.Domain.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassicCateringServiceApp.Controllers
{
    public class CategoryController : Controller
    {
        private IResponse response;
        private ICategoryRepository categoryRepository;

        public CategoryController()
        {
            categoryRepository = new CategoryRepository();
            response = new Response();
        }

        [Authorize]
        [HttpGet]
        public string Index()
        {
            var categories = categoryRepository.GetAllCategories();
            return categories.GetResponse();
        }

        [HttpPost]
        [Authorize]
        public string Add(CategoryView categoryview)
        {
            try
            {
                response = categoryRepository.Save(categoryview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response.GetResponse();
            }
        }

        [HttpPost]
        [Authorize]
        public string Edit(Int64 id)
        {
            try
            {
                response = categoryRepository.GetCategory(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Update(CategoryView categoryview)
        {
            try
            {
                response = categoryRepository.Update(categoryview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Delete(Int64 id)
        {
            try
            {
                response = categoryRepository.Delete(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }
    }
}