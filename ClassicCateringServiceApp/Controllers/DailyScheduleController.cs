﻿using ClassicCateringService.Domain.Helper;
using ClassicCateringService.Domain.Interfaces;
using ClassicCateringService.Domain.ViewModels;
using ClassicCateringServiceApp.Domain.Interfaces.IRepositories;
using ClassicCateringServiceApp.Domain.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClassicCateringServiceApp.Controllers
{
    public class DailyScheduleController : Controller
    {
        private IResponse response;
        private IDailyScheduleRepository scheduleRepository;

        public DailyScheduleController()
        {
            scheduleRepository = new DailyScheduleRepository();
            response = new Response();
        }

        [Authorize]
        [HttpGet]
        public string Index()
        {
            var schedules = scheduleRepository.GetAllDailySchedules();
            return schedules.GetResponse();
        }

        [HttpPost]
        [Authorize]
        public string Add(DailyScheduleView scheduleview)
        {
            try
            {
                response = scheduleRepository.Save(scheduleview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                return response.GetResponse();
            }
        }

        [HttpPost]
        [Authorize]
        public string Edit(Int64 id)
        {
            try
            {
                response = scheduleRepository.GetSchdeule(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Update(DailyScheduleView scheduleview)
        {
            try
            {
                response = scheduleRepository.Update(scheduleview);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

        [HttpPost]
        [Authorize]
        public string Delete(Int64 id)
        {
            try
            {
                response = scheduleRepository.Delete(id);
                return response.GetResponse();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }
    }
}