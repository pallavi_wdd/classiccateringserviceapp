﻿CateringApp.controller("UserAddController", [
    "$scope", "$http", "$stateParams", "$location", "cateringservice",
    function ($scope, $http, $stateParams, $location, cateringservice) {

        $scope.IsEditMode = false;
        $scope.User = {};
        $scope.id = $stateParams.id;

        if ($scope.id == undefined) {
            $scope.IsEditMode = false;
        } else {
            $scope.IsEditMode = true;
        }

        $scope.GetCompanies = function () {
            $http.get('/company').success(function (data) {
                $scope.Companies = data.Data;
            });
        }
        $scope.GetCompanies();

        if ($scope.IsEditMode) {
            $http.post("/user/edit/", { id: $scope.id }).success(function (data) {
                console.log(data);
                $scope.User = data.Data;
            });
        }

        $scope.Save = function () {
            if ($scope.IsEditMode) {
                $http.post("/user/update", { userview: $scope.User }).success(function (data) {
                    if (data.IsSuccess) {
                        $scope.User = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/user");
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            } else {
                $http.post("/user/Save", { userview: $scope.User }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.User = data;
                        $scope.User = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/user");
                    } else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        }
    }
]);

CateringApp.controller("UserController", ["$scope", "$http", "$location", "cateringservice",
    function ($scope, $http, $location, cateringservice) {

        $scope.tab = 1;

        $scope.Refresh = function () {
            $scope.IsInProgress = true;
            $http.post("/user", {
            }).success(function (data) {
                console.log(data);
                $scope.Users = data.Data;
            });
        };
        $scope.Refresh();

        //$scope.GetAllUsers = function () {
        //    $http.get('/user/GetAllUsers').success(function (data) {
        //        console.log(data);
        //        $scope.Users = data.Data;
        //    });
        //};
        //$scope.GetAllUsers();

        $scope.Delete = function (id) {
            if (confirm("Do you want to delete this record?")) {
                $http.post("/user/Delete/", { id: id }).success(function (response) {
                    if (response.IsSuccess) {
                        ticketgharservice.RecordStatusMessage("success", response.Message);
                        $scope.Refresh();
                    } else {
                        ticketgharservice.RecordStatusMessage("error", response.Message);
                    }
                });
            }
        };
    }]);