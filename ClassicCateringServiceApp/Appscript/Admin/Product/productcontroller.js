﻿CateringApp.controller("ProductController", ["$scope", "$http", "$stateParams", "$location", "cateringservice",
    function ($scope, $http, $stateParams, $location, cateringservice) {

        $scope.Refresh = function () {
            $http.get('/Product').success(function (data) {
                $scope.Products = data.Data;
            });
        };
        $scope.Refresh();

        $scope.Delete = function (id) {
            if (confirm('Are you sure ?')) {
                $http.post("/Product/delete/", { id: id }).success(function (data) {
                    if (data.IsSuccess) {
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $scope.Refresh();
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        };
    }]);

CateringApp.controller("ProductAddController", [
    "$scope", "$http", "$stateParams", "$location", "cateringservice", "Upload",
    function ($scope, $http, $stateParams, $location, cateringservice, Upload) {
        $scope.IsEditMode = false;
        $scope.Product = {};
        $scope.id = $stateParams.id;
        $scope.tab = 1;
        if ($scope.id == undefined) {
            $scope.IsEditMode = false;
        } else {
            $scope.IsEditMode = true;
        }

        if ($scope.IsEditMode) {
            $http.post("/product/Edit/", { id: $scope.id }).success(function (data) {
                console.log(data);
                $scope.Product = data.Data;
            });
        }

        $scope.GetProduct = function () {
            $http.get('/Product').success(function (data) {
                $scope.Products = data.Data;
            });
        };
        $scope.GetProduct();

        $scope.Save = function () {
            if ($scope.IsEditMode) {
                $http.post("/Product/Update", { productview: $scope.Product }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.Product = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/product");
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            } else {
                $http.post("/Product/Add", { productview: $scope.Product }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.Product = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/product");
                    } else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        }
    }
]);