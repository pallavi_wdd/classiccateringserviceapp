﻿CateringApp.controller("CategoryController", ["$scope", "$http", "$stateParams", "$location", "cateringservice",
    function ($scope, $http, $stateParams, $location, cateringservice) {

        $scope.Refresh = function () {
            $http.get('/category').success(function (data) {
                $scope.Categories = data.Data;
            });
        };
        $scope.Refresh();

        $scope.Delete = function (id) {
            if (confirm('Are you sure ?')) {
                $http.post("/Category/delete/", { id: id }).success(function (data) {
                    if (data.IsSuccess) {
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $scope.Refresh();
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        };
    }]);

CateringApp.controller("CategoryAddController", [
    "$scope", "$http", "$stateParams", "$location", "cateringservice", "Upload",
    function ($scope, $http, $stateParams, $location, cateringservice, Upload) {
        $scope.IsEditMode = false;
        $scope.Category = {};
        $scope.id = $stateParams.id;
        $scope.tab = 1;
        if ($scope.id == undefined) {
            $scope.IsEditMode = false;
        } else {
            $scope.IsEditMode = true;
        }

        if ($scope.IsEditMode) {
            $http.post("/Category/Edit/", { id: $scope.id }).success(function (data) {
                console.log(data);
                $scope.Category = data.Data;
            });
        }

        $scope.GetCategory = function () {
            $http.get('/Category').success(function (data) {
                $scope.Categories = data.Data;
            });
        };
        $scope.GetCategory();

        $scope.Save = function () {
            if ($scope.IsEditMode) {
                $http.post("/Category/Update", { categoryview: $scope.Category }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.Category = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/category");
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            } else {
                $http.post("/Category/Add", { categoryview: $scope.Category }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.Category = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/category");
                    } else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        }
    }
]);