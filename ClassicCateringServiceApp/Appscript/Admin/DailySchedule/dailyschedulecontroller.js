﻿CateringApp.controller("DailyScheduleController", ["$scope", "$http", "$stateParams", "$location", "cateringservice",
    function ($scope, $http, $stateParams, $location, cateringservice) {

        $scope.Refresh = function () {
            $http.get('/dailyschedule').success(function (data) {
                $scope.DailySchedules = data.Data;
            });
        };
        $scope.Refresh();

        $scope.Delete = function (id) {
            if (confirm('Are you sure ?')) {
                $http.post("/dailyschedule/delete/", { id: id }).success(function (data) {
                    if (data.IsSuccess) {
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $scope.Refresh();
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        };
    }]);

CateringApp.controller("DailyScheduleController", [
    "$scope", "$http", "$stateParams", "$location", "cateringservice", "Upload",
    function ($scope, $http, $stateParams, $location, cateringservice, Upload) {
        $scope.IsEditMode = false;
        $scope.DailySchedule = {};
        $scope.id = $stateParams.id;
        $scope.tab = 1;
        if ($scope.id == undefined) {
            $scope.IsEditMode = false;
        } else {
            $scope.IsEditMode = true;
        }

        if ($scope.IsEditMode) {
            $http.post("/DailySchedule/Edit/", { id: $scope.id }).success(function (data) {
                console.log(data);
                $scope.DailySchedule = data.Data;
            });
        }

        $scope.GetCategory = function () {
            $http.get('/DailySchedule').success(function (data) {
                $scope.DailySchedule = data.Data;
            });
        };
        $scope.GetCategory();

        $scope.Save = function () {
            if ($scope.IsEditMode) {
                $http.post("/DailySchedule/Update", { scheduleview: $scope.DailySchedule }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.DailySchedule = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/dailyschedule");
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            } else {
                $http.post("/DailySchedule/Add", { scheduleview: $scope.DailySchedule }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.DailySchedule = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/dailyschedule");
                    } else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        }
    }
]);