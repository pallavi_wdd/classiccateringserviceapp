﻿CateringApp.controller("CompanyController", ["$scope", "$http", "$stateParams", "$location", "cateringservice",
    function ($scope, $http, $stateParams, $location, cateringservice) {

        $scope.Refresh = function () {
            $http.get('/company').success(function (data) {
                $scope.Companies = data.Data;
            });
        };
        $scope.Refresh();

        $scope.Delete = function (id) {
            if (confirm('Are you sure ?')) {
                $http.post("/company/delete/", { id: id }).success(function (data) {
                    if (data.IsSuccess) {
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $scope.Refresh();
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        };
    }]);

CateringApp.controller("CompanyAddController", [
    "$scope", "$http", "$stateParams", "$location", "cateringservice", "Upload",
    function ($scope, $http, $stateParams, $location, cateringservice, Upload) {
        $scope.IsEditMode = false;
        $scope.Company = {};
        $scope.id = $stateParams.id;
        $scope.tab = 1;
        if ($scope.id == undefined) {
            $scope.IsEditMode = false;
        } else {
            $scope.IsEditMode = true;
        }

        if ($scope.IsEditMode) {
            $http.post("/Company/Edit/", { id: $scope.id }).success(function (data) {
                console.log(data);
                $scope.Company = data.Data;
            });
        }

        $scope.GetCompany = function () {
            $http.get('/Company').success(function (data) {
                $scope.Companies = data.Data;
            });
        };
        $scope.GetCompany();

        $scope.Save = function () {
            if ($scope.IsEditMode) {
                $http.post("/Company/Update", { companyview: $scope.Company }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.Company = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/company");
                    }
                    else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            } else {
                $http.post("/Company/Add", { companyview: $scope.Company }).success(function (data) {
                    if (data.IsSuccess) {
                        console.log(data);
                        $scope.Company = {};
                        cateringservice.RecordStatusMessage("success", data.Message);
                        $location.path("/company");
                    } else {
                        cateringservice.RecordStatusMessage("error", data.Message);
                    }
                });
            }
        }
    }
]);