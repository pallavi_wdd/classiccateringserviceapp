﻿CateringApp.service("cateringservice",
    ["$rootScope", "$http", "$interval", "$filter", "$document", "$q",
        function ($rootScope, $serviceHttp, $interval, $filter, $document, $q) {
            var AllMasters = {};
            var AppData = {};

            this.GetCurrentUser = function () {
                var defer = $q.defer();
                $serviceHttp.post("/Home/GetCurrentUser").success(function (data) {
                    console.log(data);
                    AppData.CurentUser = data;
                    defer.resolve(data);
                    $rootScope.$broadcast("ServiceReady", []);
                });
                //return AppData.CurentUser;
                return defer.promise;
            };

            this.GetLoggedUser = function () {
                $serviceHttp.post("/Home/GetLoggedUser").success(function (data) {
                    console.log(data);
                    AppData.LoggedUser = data;
                    $rootScope.$broadcast("ServiceReady", []);

                });
                return AppData.LoggedUser;
            };

            this.RecordStatusMessage = function (messageType, messageFromServer) {
                var msg = " ";
                var position = "right";
                var autohide = true;
                if (messageType == "success") {
                    msg = msg + messageFromServer;
                }
                if (messageType == "warning") {
                    msg = msg + messageFromServer;
                }

                if (messageType == "error") {
                    msg = msg + messageFromServer;
                }
                notif({
                    msg: "<span  class='errormessage'><b class='capitalise'>" + messageType + "! :</b>" + msg + "</span>",
                    type: messageType,
                    position: position,
                    fade: true,
                    autohide: autohide,
                    opacity: 1,
                    multiline: true
                });
            };
        }]);