﻿"use strict";

var SOFT_VER = "1.17";

var CateringApp = angular.module("classiccateringservice", ["ui.router", "ngFileUpload"]);

CateringApp.controller("HeaderController", ["$scope", "cateringservice", function ($scope, cateringservice) {
    $scope.Version = SOFT_VER;

    $scope.openNav =function() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
    }

    $scope.closeNav= function() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
    }
}]);

CateringApp.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/admin/home");

     //***********************Admin module***********************************
    var home = {
        url: '/admin/home',
        title: 'Home',
        name: 'home',
        templateUrl: "/Appscript/Admin/Home/home.html?VER=" + SOFT_VER,
    };
    $stateProvider.state(home);

    //company
    var company = {
        view: {
            url: '/company',
            title: 'companies',
            name: 'companies',
            templateUrl: "/Appscript/Admin/company/index.html?VER=" + SOFT_VER,
        },
        add: {
            url: '/company/new',
            title: 'New company',
            name: 'newcompany',
            templateUrl: "/Appscript/Admin/company/add.html?VER=" + SOFT_VER,
        },
        edit: {
            url: '/company/:id',
            title: 'Edit company',
            name: 'editcompany',
            templateUrl: "/Appscript/Admin/company/add.html?VER=" + SOFT_VER,
        }
    };
    $stateProvider.state(company.view);
    $stateProvider.state(company.add);
    $stateProvider.state(company.edit);

    //user

    var user = {

        view: {
            url: '/user',
            title: 'users',
            name: 'users',
            templateUrl: "/AppScript/Admin/user/index.html?VER=" + SOFT_VER,
        },
        add: {
            url: '/user/new',
            title: 'New user',
            name: 'newuser',
            templateUrl: "/AppScript/Admin/user/Add.html?VER=" + SOFT_VER,
        },
        edit: {
            url: '/user/:id',
            title: 'Edit user',
            name: 'edituser',
            templateUrl: "/AppScript/Admin/user/Add.html?VER=" + SOFT_VER,
        }
    };
    $stateProvider.state(user.view);
    $stateProvider.state(user.add);
    $stateProvider.state(user.edit);

    //product
    var product = {

        view: {
            url: '/product',
            title: 'products',
            name: 'products',
            templateUrl: "/Appscript/Admin/Product/index.html?VER=" + SOFT_VER,
        },
        add: {
            url: '/product/new',
            title: 'New product',
            name: 'newproduct',
            templateUrl: "/Appscript/Admin/Product/add.html?VER=" + SOFT_VER,
        },
        edit: {
            url: '/product/:id',
            title: 'Edit product',
            name: 'editproduct',
            templateUrl: "/Appscript/Admin/Product/add.html?VER=" + SOFT_VER,
        }
    };
    $stateProvider.state(product.view);
    $stateProvider.state(product.add);
    $stateProvider.state(product.edit);

    //Category
    var category = {
        view: {
            url: '/category',
            title: 'categories',
            name: 'categories',
            templateUrl: "/Appscript/Admin/Category/index.html?VER=" + SOFT_VER,
        },
        add: {
            url: '/category/new',
            title: 'New Category',
            name: 'newcategory',
            templateUrl: "/Appscript/Admin/Category/add.html?VER=" + SOFT_VER,
        },
        edit: {
            url: '/category/:id',
            title: 'Edit Category',
            name: 'editcategory',
            templateUrl: "/Appscript/Admin/Category/add.html?VER=" + SOFT_VER,
        }
    };
    $stateProvider.state(category.view);
    $stateProvider.state(category.add);
    $stateProvider.state(category.edit);

    //Daily Schedule
    var dailyschedule = {
        view: {
            url: '/dailyschedule',
            title: 'dailyschedule',
            name: 'dailyschedule',
            templateUrl: "/Appscript/Admin/DailySchedule/index.html?VER=" + SOFT_VER,
        },
        add: {
            url: '/dailyschdeule/new',
            title: 'New Daily Schdeule',
            name: 'newdailyschdeule',
            templateUrl: "/Appscript/Admin/DailySchedule/add.html?VER=" + SOFT_VER,
        },
        edit: {
            url: '/dailyschedule/:id',
            title: 'Edit Daily Schedule',
            name: 'editdailyschedule',
            templateUrl: "/Appscript/Admin/DailySchedule/add.html?VER=" + SOFT_VER,
        }
    };
    $stateProvider.state(dailyschedule.view);
    $stateProvider.state(dailyschedule.add);
    $stateProvider.state(dailyschedule.edit);


     //***********************User module***********************************
    var userhome = {
        url: '/userdashboard',
        title: 'userHome',
        name: 'userHome',
        templateUrl: "/Appscript/User/home.html?VER=" + SOFT_VER,
    };
    $stateProvider.state(userhome);

    //***********************Vendor module***********************************
    var vendorhome = {
        url: '/vendorrdashboard',
        title: 'vendorhome',
        name: 'vendorhome',
        templateUrl: "/Appscript/Vendor/home.html?VER=" + SOFT_VER,
    };
    $stateProvider.state(vendorhome);
}]);